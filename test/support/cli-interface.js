/**
 * User: curtis
 * Date: 2019-01-05
 * Time: 18:10
 * Copyright @2019 by Xraymen Inc.
 *
 * Serves our testing needs and as an outline of what a CLI interface should be structured like
 */

const {Command}=require("../../lib/command");

/* eslint-disable no-console */

/**
 * All actions supported by an implementing application
 * @type {Object<string, PigCliAction>}
 */
exports.ACTIONS={
	"test": {
		args: "<arg1> <arg2>",
		desc: "test command",
		/**
		 * @param {Command} command
		 * @returns {function(callback)}
		 */
		handler: (command)=>command._test.bind(command),
		/**
		 * @param {Array<string>} position
		 * @param {Object<string, string>} options
		 * @throws {Error} - if you want to fail validation
		 */
		validate: (position, options)=>{}
	}
};

/**
 * All supported options in addition to the preset-options defined in <link>cli.js</link>
 * @type {Array<PigCliOption>}
 */
exports.OPTIONS=[
	{
		actions: ["test"],
		args: {
			count: 1,
			name: "param"
		},
		desc: "test option",
		keys: {
			short: "n",
			long: "name"
		},
		required: false
	},
	{
		actions: ["<unsupported>"],
		args: {
			count: 0
		},
		desc: "test option",
		keys: {
			short: "x",
			long: "xxx"
		}
	}
];

/**
 * Sample interface into application specific commands
 * @type {Command} CommandInterface
 */
class CommandInterface extends Command {
	constructor(action, options, position) {
		super(options);
		this.action=action;
		this.position=position;
		this.execute=exports.ACTIONS[action].handler(this);
	}

	/**** action handlers ****/
	/**
	 * @param {Function} callback
	 * @private
	 */
	_test(callback) {
		console.log(`${this.action} handler. option.name=${this.options.name}`);
	}
}

exports.CommandInterface=CommandInterface;
