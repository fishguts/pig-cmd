/**
 * User: curtis
 * Date: 05/27/18
 * Time: 12:35 AM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const assert=require("pig-core").assert;
const queue=require("../../../lib/queue");
const {Route}=require("../../../lib/route/route");


describe("route.Route", function() {
	function _createRequest({
		headers={},
		traceId=undefined
	}={}) {
		return {
			headers: Object.assign(headers, {
				traceId
			})
		};
	}

	describe("constructor", function() {
		it("should properly construct an instance without a trace-id and without next and options", function() {
			const req=_createRequest(),
				res={},
				instance=new Route(req, res);
			assert.strictEqual(instance.req, req);
			assert.strictEqual(instance.res, res);
			assert.strictEqual(instance.next, undefined);
			assert.ok(instance.traceId.startsWith("urn:rte"));
			assert.deepEqual(_.omit(instance.options, ["traceId"]), {});
		});

		it("should properly construct an instance with overrides", function() {
			const req=_createRequest(),
				res={},
				instance=new Route(req, res, "next", {
					traceId: "traceId",
					verbose: true
				});
			assert.strictEqual(instance.next, "next");
			assert.strictEqual(instance.traceId, "traceId");
			assert.deepEqual(instance.options, {
				traceId: "traceId",
				verbose: true
			});
		});
	});

	describe("getOption", function() {
		it("should properly return an existing option", function() {
			const instance=new Route(_createRequest(), {}, undefined, {
				verbose: true
			});
			assert.strictEqual(instance.getOption("verbose"), true);
		});

		it("should properly default an option that does not exist", function() {
			const instance=new Route(_createRequest(), {});
			assert.strictEqual(instance.getOption("verbose", false), false);
		});
	});

	describe("createCommandQueue", function() {
		it("should by default create a series-queue", function() {
			const instance=new Route(_createRequest(), {});
			assert.ok(instance.createCommandQueue() instanceof queue.SeriesQueue);
		});
	});
});
