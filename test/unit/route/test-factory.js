/**
 * User: curtis
 * Date: 05/27/18
 * Time: 12:35 AM
 * Copyright @2018 by Xraymen Inc.
 */

const assert=require("pig-core").assert;
const proxy=require("pig-core").proxy;
const factory=require("../../../lib/route/factory");


describe("route.factory", function() {
	beforeEach(function() {
		proxy.log.stub();
	});

	afterEach(function() {
		proxy.log.unstub();
	});

	describe("create", function() {
		it("should properly create and return a handler function", function() {
			assert.strictEqual(typeof(factory.create(String)), "function");
		});
	});

	describe("_execute", function() {
		// this guy will not be so easy to unit test. todo later
	});
});
