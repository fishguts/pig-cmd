/**
 * User: curtis
 * Date: 05/27/18
 * Time: 12:35 AM
 * Copyright @2018 by Xraymen Inc.
 */

const assert=require("pig-core").assert;
const proxy=require("pig-core").proxy;
const {Command}=require("../../lib/command");
const {
	executeCommand,
	ParallelQueue,
	SeriesQueue
}=require("../../lib/queue");


class TestCommand extends Command {
	execute(callback) {
		process.nextTick(callback);
	}
}

class TestPromiseCommand extends Command {
	execute() {
		return Promise.resolve();
	}
}

class TestErrorCommand extends Command {
	execute(callback) {
		process.nextTick(callback, new Error("failed"));
	}
}


describe("queue", function() {
	beforeEach(function() {
		proxy.log.stub();
	});

	afterEach(function() {
		proxy.log.unstub();
	});

	describe("Queue", function() {
		it("should successfully handle a straight up callback command", function(done) {
			const series=new SeriesQueue();
			series.addCommand(new TestCommand());
			series.execute(function(error) {
				assert.ifError(error);
				done();
			});
		});

		it("should successfully handle a straight up Promise command", function(done) {
			const series=new SeriesQueue();
			series.addCommand(new TestPromiseCommand());
			series.execute(function(error) {
				assert.ifError(error);
				done();
			});
		});

		it("should return error if error policy is 'error'", function(done) {
			const series=new SeriesQueue();
			series.addCommand(new TestErrorCommand());
			series.execute(function(error) {
				assert.strictEqual(error.message, "failed");
				assert.strictEqual(proxy.log.error.callCount, 1);
				done();
			});
		});

		it("should not return error if error policy is not 'error'", function(done) {
			const series=new SeriesQueue({errorPolicy: "info"});
			series.addCommand(new TestErrorCommand());
			series.execute(function(error) {
				assert.ifError(error);
				assert.strictEqual(proxy.log.info.callCount, 1);
				done();
			});
		});

		it("should not return error if error policy is not 'error'", function(done) {
			const series=new SeriesQueue({errorPolicy: "info"});
			series.addCommand(new TestErrorCommand());
			series.execute(function(error) {
				assert.ifError(error);
				assert.strictEqual(proxy.log.info.callCount, 1);
				done();
			});
		});
	});

	describe("SeriesQueue", function() {
		it("should construct properly", function() {
			const series=new SeriesQueue();
			assert.deepEqual(series.queue, []);
		});

		it("should execute queue", function(done) {
			const series=new SeriesQueue();
			series.addCommand(new TestCommand());
			series.addCommand(new TestCommand());
			series.execute(function(error) {
				assert.ifError(error);
				done();
			});
		});
	});

	describe("ParallelQueue", function() {
		it("should construct properly", function() {
			const parallel=new ParallelQueue();
			assert.deepEqual(parallel.queue, []);
		});

		it("should execute queue", function(done) {
			const parallel=new ParallelQueue();
			parallel.addCommand(new TestCommand());
			parallel.addCommand(new TestCommand());
			parallel.execute(function(error) {
				assert.ifError(error);
				done();
			});
		});
	});
});
