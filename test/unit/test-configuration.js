/**
 * User: curtis
 * Date: 11/18/18
 * Time: 9:55 PM
 * Copyright @2018 by Xraymen Inc.
 */

const assert=require("assert");
const configuration=require("../../lib/configuration");

describe("configuration", function() {
	it("pig-core should return proper reference to pig-core", function() {
		assert.strictEqual(configuration.core, require("pig-core"));
	});

	it("properties should properly update", function() {
		[
			["debug", false, true],
			["isLowerEnv", false, true],
			["name", "one", "two"],
			["nodenv", "local", "dev"],
			["port", undefined, 8100],
			["verbose", false, true]
		].forEach(([name, value1, value2])=>{
			// let's make sure the default returns an appropriate value
			assert.strictEqual(typeof(configuration[name]), typeof(value1));
			// now test our update mechanism
			configuration[name]=value1;
			assert.strictEqual(configuration[name], value1);
			configuration[name]=value2;
			assert.strictEqual(configuration[name], value2);
		});
	});
});
