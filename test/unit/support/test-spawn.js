/**
 * User: curtis
 * Date: 2018-12-06
 * Time: 22:41
 * Copyright @2018 by Xraymen Inc.
 */

const assert=require("assert");
const spawn=require("../../../lib/support/spawn");

describe("support.spawn", function() {
	describe("NodeServer", function() {
		describe("constructor", function() {
			it("should instantiate properly", function() {
				const instance=new spawn.NodeServer({
					logPrefix: "prefix",
					nodenv: "nodenv",
					serverArgs: "args",
					serverPath: "path",
					serverRoot: "root",
					verbose: true
				});
				assert.strictEqual(instance._logPrefix, "prefix");
				assert.strictEqual(instance._nodenv, "nodenv");
				assert.strictEqual(instance._serverArgs, "args");
				assert.strictEqual(instance._serverPath, "path");
				assert.strictEqual(instance._serverRoot, "root");
				assert.strictEqual(instance._verbose, true);
			});
		});

		describe("getSingleton", function() {
			it("should find instance once created", function() {
				const instance1=spawn.NodeServer.getSingleton({
						serverPath: "path",
						serverRoot: "root"
					}),
					instance2=spawn.NodeServer.getSingleton({
						serverPath: "path",
						serverRoot: "root"
					});
				assert.strictEqual(instance1, instance2);
			});
		});
	});
});
