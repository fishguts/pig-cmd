/**
 * User: curtis
 * Date: 05/27/18
 * Time: 12:35 AM
 * Copyright @2018 by Xraymen Inc.
 */

const fs=require("fs-extra");
const assert=require("pig-core").assert;
const proxy=require("pig-core").proxy;
const {PigError}=require("pig-core").error;
const {
	CopyPathCommand,
	DeletePathCommand,
	EnsureNotPathCommand,
	EnsurePathCommand,
	MovePathCommand,
	PathExistsCommand,
	PathNotExistsCommand,
	ReadFileCommand
}=require("../../lib/file");

const TEST_ROOT="test/playground";


describe("file", function() {
	beforeEach(function() {
		fs.removeSync(TEST_ROOT);
		proxy.log.stub();
	});

	afterEach(function() {
		proxy.log.unstub();
		fs.removeSync(TEST_ROOT);
	});

	describe("PathExistsCommand", function() {
		it("should successfully detect when a path exists", function(done) {
			const instance=new PathExistsCommand("./package.json");
			instance.execute(done);
		});

		it("should return a 404 error if path does not exist", function(done) {
			const instance=new PathExistsCommand("./absent.file");
			instance.execute(function(error) {
				assert.ok(error instanceof PigError);
				assert.strictEqual(error.statusCode, 404);
				done();
			});
		});
	});

	describe("MovePathCommand", function() {
		it("should not do anything if the source is the target", function(done) {
			const move=new MovePathCommand("file", "file");
			move.execute(done);
		});

		it("should properly create and move a directory to a nested directory", function(done) {
			const directory=TEST_ROOT,
				subdirectory=`${directory}/subdirectory`,
				move=new MovePathCommand(directory, subdirectory);
			fs.ensureDirSync(directory);
			fs.ensureDirSync(subdirectory);
			fs.writeJsonSync(`${directory}/dummy.json`, {});
			move.execute(function(error) {
				assert.equal(error, null);
				assert.equal(fs.pathExistsSync(`${directory}/dummy.json`), false);
				assert.equal(fs.pathExistsSync(`${subdirectory}/dummy.json`), true);
				done();
			});
		});
	});
});
