/**
 * User: curtis
 * Date: 11/14/18
 * Time: 8:18 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const assert=require("pig-core").assert;
const index=require("../../lib/index");

describe("index", function() {
	it("should properly import each public module", function() {
		const set=new Set(),
			moduleNames=[
				"cli",
				"command",
				"configuration",
				"data",
				"express",
				"file",
				"http.request",
				"http.response",
				"queue",
				"route.factory",
				"route.Route"
			];
		moduleNames.forEach((name)=>{
			const moduleLoad=_.get(index, name),
				moduleCache=_.get(index, name);
			assert.ok(_.includes(["function", "object"], typeof(moduleLoad)), `module=${name} failed`);
			assert.strictEqual(moduleLoad, moduleCache);
			if(set.has(moduleLoad)) {
				assert.ok(_.includes(["log", "direct.log.client"], name), `name=${name} already loaded?`);
			} else {
				set.add(moduleLoad);
			}
		});
		// note: -1 because log will load the same module twice
		assert.equal(moduleNames.length, set.size);
	});
});
