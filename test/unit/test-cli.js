/**
 * User: curtis
 * Date: 2018-11-27
 * Time: 21:58
 * Copyright @2018 by Xraymen Inc.
 */

const proxy=require("pig-core").proxy;
const cli=require("../../lib/cli");
const cliInterface=require("../support/cli-interface");

/**
 * These are meant for interactive development with developer.
 */
describe("cli", function() {
	describe("run", function() {
		const argsRestore=process.argv;

		beforeEach(()=>{
			proxy.stub(process, "exit");
		});

		afterEach(()=>{
			process.argv=argsRestore;
			proxy.unstub();
		});

		it("should show general help if -h is specified", function() {
			process.argv=["pigcmd.js", "-h"];
			cli.run(cliInterface);
		});

		it("should show general help if --help is specified", function() {
			process.argv=["pigcmd.js", "--help"];
			cli.run(cliInterface);
		});

		it("should show action specific help if --help is specified with an action", function() {
			process.argv=["pigcmd.js", "--help", "test"];
			cli.run(cliInterface);
		});

		it("should log general error and usage if no action is specified", function() {
			process.argv=["pigcmd.js"];
			cli.run(cliInterface);
		});

		it("should log error if unsupported option is specified", function() {
			process.argv=["pigcmd.js", "test", "--missing"];
			cli.run(cliInterface);
		});

		it("should log error if option unsupported by action is specified", function() {
			process.argv=["pigcmd.js", "test", "--xxx"];
			cli.run(cliInterface);
		});

		it("should log error if option is missing required param", function() {
			process.argv=["pigcmd.js", "test", "--name"];
			cli.run(cliInterface);
		});

		it("should successfully run properly formatted", function() {
			process.argv=["pigcmd.js", "test", "--name=dog boy"];
			cli.run(cliInterface);
		});
	});
});
