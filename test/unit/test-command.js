/**
 * User: curtis
 * Date: 05/27/18
 * Time: 12:23 PM
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const sinon=require("sinon");
const assert=require("pig-core").assert;
const {PigError}=require("pig-core").error;
const proxy=require("pig-core").proxy;
const {
	AsyncCommand,
	Command,
	History,
	HookCommand,
	SyncCommand
}=require("../../lib/command");


describe("command", function() {
	beforeEach(function() {
		proxy.log.stub();
	});

	afterEach(function() {
		proxy.log.unstub();
	});

	describe("AsyncCommand", function() {
		it("should allocate properly", function(done) {
			const handler=(callback)=>{
					process.nextTick(callback);
				},
				instance=new AsyncCommand(handler);
			assert.strictEqual(instance.function, handler);
			instance.execute(done);
		});
	});

	describe("Command", function() {
		describe("execute", function() {
			it("should throw exception at this level", function() {
				const command=new Command();
				assert.throws(command.execute.bind(command));
			});
		});

		describe("getName", function() {
			it("should return the name of the class", function() {
				const command=new Command();
				assert.strictEqual(command.getName(), "Command");
			});
		});

		describe("log", function() {
			it("should not log messages if severity is 'none'", function() {
				const command=new Command({
					logLevel: {
						error: "none",
						other: "none"
					}
				});
				command.log({
					message: "test",
					severity: "info"
				});
				command.log({
					message: "test",
					severity: "error"
				});
				assert.strictEqual(proxy.log.info.callCount, 0);
				assert.strictEqual(proxy.log.error.callCount, 0);
			});

			it("should log messages if severity is not 'none'", function() {
				const command=new Command({
					logLevel: {
						error: "min",
						other: "min"
					}
				});
				command.log({
					message: "test",
					severity: "info"
				});
				command.log({
					message: "test",
					severity: "error"
				});
				assert.strictEqual(proxy.log.info.callCount, 1);
				assert.strictEqual(proxy.log.error.callCount, 1);
				assert.deepEqual(proxy.log.info.getCall(0).args, ["Command: test"]);
				assert.deepEqual(proxy.log.error.getCall(0).args, ["Command: test"]);
			});

			it("should properly log non-errors with details", function() {
				const command=new Command({
					logLevel: {
						other: "max"
					}
				});
				command.log({
					message: "test",
					severity: "info"
				});
				assert.strictEqual(proxy.log.info.getCall(0).args[0], "Command: test");
				assert.ok(_.isPlainObject(proxy.log.info.getCall(0).args[1].meta));
			});

			it("should properly log errors with details", function() {
				const command=new Command({
						logLevel: {
							error: "max"
						}
					}),
					error=new PigError({
						instance: command,
						message: "failed"
					});
				command.log({
					error,
					message: "unhandled error",
					severity: "error"
				});
				assert.strictEqual(proxy.log.error.getCall(0).args[0], "Command: unhandled error - failed");
				assert.ok(_.isPlainObject(proxy.log.error.getCall(0).args[1].meta));
			});
		});

		describe("toString", function() {
			it("should format a lonely message", function() {
				const command=new Command({}),
					text=command.toString({
						message: "test"
					});
				assert.strictEqual(text, "Command: test");
			});

			it("should format with prefix and suffix", function() {
				const command=new Command({}),
					text=command.toString({
						message: "test",
						prefix: "prefix.",
						suffix: ".suffix"
					});
				assert.strictEqual(text, "prefix.Command: test.suffix");
			});

			it("should format with a method", function() {
				const command=new Command({});
				assert.strictEqual(command.toString({
					message: "test",
					method: "method"
				}), "Command.method(): test");
				assert.strictEqual(command.toString({
					message: "test",
					method: "method()"
				}), "Command.method(): test");
			});

			it("should format an error", function() {
				const command=new Command({}),
					text=command.toString({
						error: new Error("error")
					});
				assert.strictEqual(text, "Command: error");
			});
		});
	});

	describe("HookCommand", function() {
		it("should allocate properly", function() {
			const hook=sinon.stub(),
				instance=new HookCommand(hook);
			assert.strictEqual(instance.execute, hook);
		});
	});

	describe("SyncCommand", function() {
		it("should allocate properly", function(done) {
			const instance=new SyncCommand(_.noop);
			assert.strictEqual(instance.function, _.noop);
			instance.execute(done);
		});
	});
});
