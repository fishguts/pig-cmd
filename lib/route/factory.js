/**
 * User: curtis
 * Date: 05/27/18
 * Time: 9:37 PM
 * Copyright @2018 by Xraymen Inc.
 *
 * @module pig-cmd/route/factory
 *
 * Connects handlers and derived instances of <code>Route</code>
 */

const _=require("lodash");
const constant=require("pig-core").constant;
const {PigError}=require("pig-core").error;
const {errorToString}=require("pig-core").format;
const log=require("pig-core").log;
const validator=require("pig-core").validator;
const {HttpSendStatusCommand}=require("../http/response");


/**
 * Returns a factory function that creates a route handler for the specified class
 * @param {Route.constructor} clss
 * @returns {Route}
 */
exports.create=function(clss) {
	return exports._execute.bind(null, clss);
};

/********************* Private Interface *********************/

/**
 * Creates and executes a route. Wraps with error handling
 * @param {Route.constructor} clss
 * @param {IncomingMessage} req
 * @param {ServerResponse} res
 * @param {Function} next
 * @returns {Route|undefined}
 */
exports._execute=function(clss, req, res, next) {
	/**
	 * Optionally logs an error, but always responds to res with error response
	 * @param {Error} error
	 * @param {Boolean} writeToLog
	 */
	function _processError(error, writeToLog=true) {
		if(writeToLog) {
			let message,
				meta=Object.assign({
					class: clss.name
				}, _.pick(req, ["originalUrl", "method", "params", "headers"])),
				severity=constant.severity.ERROR;
			// we differentiate here. If it's a PigError then we created it. If it's not then:
			//	- it should be and we should change it
			//	- or something unexpected happened and we should fix it
			if(error instanceof PigError) {
				message=errorToString(error);
			} else {
				error=new PigError({
					details: error.message,
					message: "unexpected error thrown",
					error
				});
				message=errorToString(error, {
					details: true,
					stack: true
				});
			}
			log[severity](`route.factory._execute: ${message}`, {meta});
		}
		const command=new HttpSendStatusCommand(res, {error});
		command.execute(_.noop);
	}

	try {
		/* eslint-disable new-cap */
		const route=new clss(req, res, next);
		exports._validate(route);
		route.init(function(error) {
			if(error) {
				// we assume responsibility for logging errors during init
				_processError(error);
			} else {
				try {
					route.execute(function(error) {
						if(error) {
							// note: the queue will have logged this error
							_processError(error, false);
						}
					});
				} catch(error) {
					_processError(error);
				}
			}
		});
		return route;
	} catch(error) {
		_processError(error);
	}
};


/**
 * Validates the request
 * @param {Route} route
 * @throws {Error}
 * @private
 */
exports._validate=function(route) {
	const schema=route.schema;
	if(schema) {
		validator.validateData(route.req, schema);
	}
};
