/**
 * User: curtis
 * Date: 05/27/18
 * Time: 11:10 PM
 * Copyright @2018 by Xraymen Inc.
 *
 * @module pig-cmd/route
 */

const _=require("lodash");
const urn=require("pig-core").urn;
const {SeriesQueue}=require("../queue");

/**
 * Base class for all routes.
 */
class Route {
	/**
	 * Creates instance and stashes req and res and next
	 * @param {IncomingMessage} req
	 * @param {ServerResponse} res
	 * @param {Function} next
	 * @param {Object} options
	 */
	constructor(req, res, next=undefined, options={}) {
		this.req=req;
		this.res=res;
		this.next=next;
		this.options=Object.assign({
			traceId: req.headers["traceid"] || req.headers["trace-id"] || urn.create("rte")
		}, options);
	}

	/**
	 * Gets the trace id that we either received or created for this route
	 * @returns {string}
	 */
	get traceId() { return this.options.traceId; }

	/**
	 * All routes that have a body should return a schema for validation. If none is supplied then no schema validation happens.
	 * @returns {string|Object|null} - a path to the file based schema or schema object
	 */
	get schema() { return null; }

	/**
	 * Gets optional setting
	 * @param {string} name
	 * @param {*} dfault
	 * @returns {*}
	 */
	getOption(name, dfault) {
		return _.get(this.options, name, dfault);
	}

	/**
	 * Constructs a series queue set with verbose, traceId and whatever else you want in the mix.
	 * @param {Class} QueueClass - queue class to instantiate
	 * @param {Object} properties - optional and additional properties to merge into <code>this.options</code> when creating the queue
	 * @returns {SeriesQueue}
	 */
	createCommandQueue({
		QueueClass=SeriesQueue,
		...properties
	}={}) {
		const options=Object.assign(_.pick(this.options, ["traceId", "verbose"]), properties);
		return new QueueClass(options);
	}

	/**** Our Virtual Interface ****/
	/**
	 * Opportunity to do some asynchronous stuff before we get the party started.
	 * By default he does nothing.
	 * @param {Function} callback
	 */
	init(callback) {
		process.nextTick(callback);
	}

	/**
	 * Get the party started.
	 * @param {Function} callback
	 * @abstract
	 */
	execute(callback) {
		throw new Error("implement");
	}
}

exports.Route=Route;

