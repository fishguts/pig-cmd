/**
 * User: curtis
 * Date: 05/27/18
 * Time: 1:22 PM
 * Copyright @2018 by Xraymen Inc.
 *
 * @module pig-cmd/cli
 *
 * A command line utility for doing the things that need to be done
 */

/* eslint-disable no-console */


const _=require("lodash");
const path=require("path");
const format=require("pig-core").format;
const {PigError}=require("pig-core").error;
const {SyncCommand}=require("./command");
const {SeriesQueue}=require("./queue");

/**
 * Our own reserved options that apply to all commands
 * @type {Array<PigCliOption>}
 */
const PRESET_OPTIONS=[
	{
		args: {
			count: 0
		},
		desc: "print general help or help for a specific command",
		keys: {
			short: "h",
			long: "help"
		}
	},
	{
		args: {
			count: 0
		},
		desc: "minimal output",
		keys: {
			short: "q",
			long: "quiet"
		}
	},
	{
		args: {
			count: 0
		},
		desc: "maximum output",
		keys: {
			short: "v",
			long: "verbose"
		}
	}
];


/**
 * try/catch and see what is on our command line and do what it wants us to do if we can do it.
 * @param {PigCliInterface} iface
 * @param {Command} SetupCommand - optional command to run to do setup stuff if the runway is clear (as in SetupEnvironment)
 */
exports.run=function(iface, {
	setupCommand=null
}={}) {
	const OPTIONS=PRESET_OPTIONS
		.concat(iface.OPTIONS);

	/**
	 * Parses the command line and stores his goodies
	 */
	class CommandLine {
		/**
		 * @throws {Error}
		 */
		constructor() {
			this._runner=null;
			this._action=null;
			this._options={};
			this._position=[];
			this._parse();
		}

		/**
		 * @returns {string}
		 */
		get runner() { return this._runner; }
		/**
		 * @returns {string}
		 */
		get action() { return this._action; }
		/**
		 * @returns {Object<string, string>}
		 */
		get options() { return this._options; }
		/**
		 * @returns {Array<string>}
		 */
		get position() { return this._position; }

		/**** Private Interface ****/
		/**
		 * Parses the command line and packs goodies into `this`
		 * @private
		 */
		_parse() {
			let index=_.includes(process.argv[0], "node") ? 1 : 0;
			while(index<process.argv.length) {
				if(_.startsWith(process.argv[index], "-")) {
					index=this._processOption(index);
				} else {
					this._position.push(process.argv[index]);
					index++;
				}
			}
			this._runner=path.parse(this._position.shift()).base;
			this._action=this._position.shift();
		}

		/**
		 * Processes the option that should be at <param>index</param>
		 * @param {number} index
		 * @returns {number}
		 * @private
		 */
		_processOption(index) {
			if(_.startsWith(process.argv[index], "--")) {
				const match=process.argv[index].match(/--([^=]+)=?(.*)/),
					key=match[1],
					value=match[2],
					option=findOption(key);
				if(option==null) {
					throw new Error(`unknown option "${key}"`);
				} else if(option.args.count===0 && value.length!==0) {
					throw new Error(`option "${key}" does not take an argument`);
				} else if(option.args.count===1 && value.length===0) {
					throw new Error(`option "${key}" missing required argument`);
				} else {
					this._options[option.keys.long]=_.isEmpty(value)
						? true
						: value;
				}
			} else {
				const match=process.argv[index].match(/-(\S+)/),
					key=match[1],
					option=findOption(key);
				if(!option) {
					throw new Error(`unknown option "${key}"`);
				} else if(process.argv.length<index+option.args.count) {
					throw new Error(`option "${key}" missing required argument`);
				} else {
					this._options[option.keys.long]=(option.args.count===0)
						? true
						: process.argv[++index];
				}
			}
			return index+1;
		}
	}

	/**
	 * Takes a command line, processes it and hooks it up with a command to do what needs to be done
	 */
	class Runner {
		/**
		 * @param {CommandLine} commandLine
		 */
		constructor(commandLine) {
			const silent="quiet" in commandLine.options;
			const verbose="verbose" in commandLine.options;
			this._line=commandLine;
			this._queue=new SeriesQueue({
				logLevel: {
					error: verbose ? "max" : "none",
					other: silent ? "none" : (verbose ? "max" : "min")
				},
				silent: silent,
				verbose: verbose
			});
		}

		/**
		 * Runs the command and waits for completion before exiting process.
		 * @param {Function} callback
		 */
		run(callback) {
			this._validate();
			this._populateExecutionQueue();
			this._queue.execute(callback);
		}

		/**
		 * Converts the request into a series of commands
		 * @throw {Error}
		 */
		_populateExecutionQueue() {
			if("help" in this._line.options) {
				this._queue.addCommand(new SyncCommand(reportUsage.bind(null, this._line.action)));
			} else {
				if(setupCommand) {
					this._queue.addCommand(setupCommand);
				}
				this._queue.addCommand(new iface.CommandInterface(
					this._line.action,
					Object.assign({}, this._queue.options, this._line.options),
					this._line.position
				));
			}
		}

		/**
		 * Converts the request into a series of commands
		 * @throw {Error}
		 */
		_validate() {
			if(!("help" in this._line.options)) {
				if(!this._line.action) {
					throw new Error(`${this._line.runner}: no command specified`);
				}
				const action=findAction(this._line.action);
				if(!action) {
					throw new Error(`${this._line.runner}: unknown command "${this._line.action}"`);
				}
				if(("quiet" in this._line.options) && ("verbose" in this._line.options)) {
					throw new PigError({
						action: this._line.action,
						message: `${this._line.runner} ${this._line.action}: options "verbose" and "quiet" conflict`
					});
				}
				const eligibleOptions=OPTIONS.filter(option=>_.isEmpty(option.actions) || _.includes(option.actions, this._line.action)),
					unsupported=_.difference(Object.keys(this._line.options), _.map(eligibleOptions, "keys.long")),
					missing=_.difference(Object.keys(_.filter(this._line.options, {required: true})), _.map(eligibleOptions, "keys.long"));
				if(unsupported.length>0) {
					throw new PigError({
						action: this._line.action,
						message: `${this._line.runner} ${this._line.action}: options ${unsupported.map(name=>`--${name}`).join("|")} not supported by "${this._line.action}" command`
					});
				}
				if(missing.length>0) {
					throw new PigError({
						action: this._line.action,
						message: `${this._line.runner} ${this._line.action}: required options ${missing.map(name=>`--${name}`).join("|")} for "${this._line.action}" are missing`
					});
				}
				try {
					action.validate(this._line.position, this._line.options);
				} catch(error) {
					throw new PigError({
						action: this._line.action,
						message: `${this._line.runner} ${this._line.action}: ${error.message}`
					});
				}
			}
		}
	}

	/**
	 * Finds the action
	 * @param {string} name
	 * @returns {PigCliAction|undefined}
	 */
	function findAction(name) {
		return iface.ACTIONS[name];
	}

	/**
	 * Finds the option by key - short or long
	 * @param {string} key
	 * @returns {PigCliOption|undefined}
	 */
	function findOption(key) {
		return _.find(OPTIONS, option=>_.includes(Object.values(option.keys), key));
	}

	/**
	 * Report usage of all actions or for a specific action
	 * @param {string} actionName - specific action to show usage for
	 */
	function reportUsage(actionName=undefined) {
		/**
		 * @param {Array<PigCliOption>} options
		 * @returns {string}
		 */
		function _formatOptions(options) {
			if(_.isEmpty(options)) {
				return "";
			} else {
				options=_.sortBy(options, "keys.long");
				return options.map(option=>{
					if(option.args.count===0) {
						return `[-${option.keys.short}|--${option.keys.long}]`;
					} else {
						return `[-${option.keys.short} <${option.args.name}>|--${option.keys.long}=<${option.args.name}>]`;
					}
				}).join(" ");
			}
		}

		if(actionName) {
			const action=findAction(actionName),
				options=_.chain(OPTIONS)
					.filter(option=>_.isEmpty(option.actions) || _.includes(option.actions, actionName))
					.sortBy("keys.long")
					.value();
			console.info(`Description: ${action.desc}`);
			console.info(`Usage: pigcmd.js ${_formatOptions(OPTIONS)} ${actionName} ${action.args || ""}`);
			options.forEach(option=>{
				console.info(`   -${option.keys.short}|--${option.keys.long}: ${option.desc}`);
			});
		} else {
			console.info(`Usage: pigcmd.js ${_formatOptions(OPTIONS)} <command> [<args>]`);
			OPTIONS.forEach(option=>{
				console.info(`   -${option.keys.short}|--${option.keys.long}: ${option.desc}`);
			});
			console.info("\nSupported commands:");
			Object.keys(iface.ACTIONS).sort().forEach(actionName=>{
				const action=findAction(actionName);
				console.info(`   ${actionName}: ${action.desc}`);
			});
		}
		console.info("");
	}

	try {
		const commandLine=new CommandLine(),
			runner=new Runner(commandLine);
		runner.run(function(error) {
			if(error) {
				console.error(`${commandLine.runner} ${commandLine.action} failed: ${format.errorToString(error)}\n`);
				process.exit(1);
			} else {
				process.exit(0);
			}
		});
	} catch(error) {
		console.error(`Error: ${format.errorToString(error)}`);
		reportUsage(error.action);
		process.exit(1);
	}
};
