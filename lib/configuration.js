/**
 * User: curtis
 * Date: 11/14/18
 * Time: 11:56 AM
 * Copyright @2018 by Xraymen Inc.
 *
 * @module pig-cmd/configuration
 *
 * We pulled commands out of our applications. Within our apps we had the advantage of exposing our
 * command architecture to the application configuration. We loose that here. This guy facilitates
 * a subset of the power of application configuration. He manages a single configuration that you
 * the user of this lib may configure. When using this lib you will want to configure this guy
 */

const storage={
	defaults: {
		core: undefined,
		debug: undefined,
		isLowerEnv: undefined,
		name: undefined,
		nodenv: undefined,
		port: undefined,
		verbose: undefined
	}
};

module.exports={
	/**
	 * Why is this guy doing here, you ask!? It's for keeping order and ensuring that peer-dependency relationships are proper.
	 * It's so that our implementing projects may include this guy in unit tests to make sure our pig-core is the same as theirs.
	 * @returns {module:pig-core}
	 */
	get core() {
		return storage.defaults.core || (storage.defaults.core=require("pig-core"));
	},

	/**
	 * Whether application is in debug mode or not
	 * @returns {Boolean}
	 */
	get debug() {
		return (storage.defaults.debug===undefined)
			? Boolean(process.env.DEBUG)
			: storage.defaults.debug;
	},
	set debug(value) {
		storage.defaults.debug=value;
	},

	/**
	 * Whether application is in debug mode or not
	 * @returns {Boolean}
	 */
	get isLowerEnv() {
		return (storage.defaults.isLowerEnv===undefined)
			? this.debug
			: storage.defaults.isLowerEnv;
	},
	set isLowerEnv(value) {
		storage.defaults.isLowerEnv=value;
	},

	/**
	 * Name of the application
	 * @returns {string}
	 */
	get name() {
		return (storage.defaults.name===undefined)
			? process.env.NAME || "untitled"
			: storage.defaults.name;
	},
	set name(value) {
		storage.defaults.name=value;
	},

	/**
	 * Environment name
	 * @returns {string}
	 */
	get nodenv() {
		return (storage.defaults.nodenv===undefined)
			? process.env.NODENV || "production"
			: storage.defaults.nodenv;
	},
	set nodenv(value) {
		storage.defaults.nodenv=value;
	},

	/**
	 * Server port (if a server). We default to undefined.
	 * @returns {number|undefined}
	 */
	get port() {
		return storage.defaults.port;
	},
	set port(value) {
		storage.defaults.port=value;
	},

	/**
	 * @returns {Boolean}
	 */
	get verbose() {
		return (storage.defaults.verbose===undefined)
			? Boolean(process.env.VERBOSE)
			: storage.defaults.verbose;
	},
	set verbose(value) {
		storage.defaults.verbose=value;
	},

	/**************************************************************************************************************************
	 * Regarding pig-core: We don't want to have our own instance of pig-core. We want to share the host application's version
	 * for a number of reasons:
	 * 1. <code>pig-core.notification</code> and <code>pig-core.valiator</code> are implemented as singletons. We want to
	 *   interact with the same instances as does our host
	 * 2. prototype resolution. We want the types that we create and test in here to be the same as those that our host
	 *   operates with.  A key example is <code>PigError</code>.
	 * I had been doing some custom node resolution but have since learned about npm's peerDependencies which does exactly
	 * what we want.  So I am tearing out the custom resolution.  See commit prior to 11/29 in you want to resurrect it.
	 **************************************************************************************************************************/
};
