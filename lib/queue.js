/**
 * User: curtis
 * Date: 05/27/18
 * Time: 12:00 PM
 * Copyright @2018 by Xraymen Inc.
 *
 * @module pig-cmd/queue
 */

const _=require("lodash");
const async=require("async");
const urn=require("pig-core").urn;
const {PigError}=require("pig-core").error;
const constant=require("pig-core").constant;
const {Command, History}=require("./command");
const configuration=require("./configuration");


/**
 * Base class for any command queue. One advantage (or disadvantage) is that commands will inherit a number
 * of options from the queue: logLevel, traceId, verbose, etc. See options.
 * @abstract
 * @typedef {Command} Queue
 */
class Queue extends Command {
	/**
	 * @param {Object} autoMerge - properties that are automatically merged into each command this queue processes.
	 * @param {{error:PigLogDetail, other:PigLogDetail}} logLevel - controls level of detail that gets looged. Always auto-merged into commands.
	 * @param {string} traceId - uses this one or creates one.
	 * @param {Boolean} verbose. We will also auto-merge verbose into commands
	 * @param {Object} options:
	 *  - logLevel
	 */
	constructor({
		autoMerge={},
		logLevel=undefined,
		traceId=urn.create("que"),
		verbose=configuration.verbose,
		...options
	}={}) {
		super({
			autoMerge,
			logLevel,
			traceId,
			verbose,
			...options
		});
		_.merge(this.options, {
			autoMerge: {
				logLevel: this.options.logLevel,
				traceId: this.options.traceId,
				verbose: this.options.verbose
			}
		});
		this.queue=[];
	}

	/**
	 * Adds command to this queue. They are executed in order.
	 * @param {Command} command
	 */
	addCommand(command) {
		this.queue.push(command);
	}

	/**
	 * Adds command to this queue. They are executed in order.
	 * @param {[Command]} commands
	 */
	addCommands(commands) {
		_.forEach(commands, (command)=>this.queue.push(command));
	}

	/**** Protected/Private Interface ****/
	/**
	 * Executes commands in this queue using executor. It makes sure that all command share the same
	 * traceId - either the one in options or one that it will create.
	 *    - logging: logs commands if the queue is set to verbose and the command is set to verbose
	 * @param {Function} executor function(iterable, callback)
	 * @param {Function} callback function(error, history)
	 * @protected
	 */
	_execute(executor, callback=_.noop) {
		const self=this,
			history=new History(this);
		// The "masterCommand" commands are those command that are in our queue. And can be seen by Command.commands()
		// and our own Queue._expand they may be translated into one or more child commands.
		executor(self.queue, function(masterCommand, done) {
			let children;
			// 1. translate the parent command into children commands
			try {
				children=self._expand({
					command: masterCommand,
					history
				});
			} catch(error) {
				masterCommand.log({
					error,
					message: `exception thrown during ${self.getName()}.commands()`,
					stack: true
				});
				masterCommand.error=error;
				// We assume that at this level that the exception was that something went awry and is always an error.
				return process.nextTick(done, error);
			}
			_.merge(masterCommand.options, self.options.autoMerge);
			// 2. execute in series each of the master's children commands
			if(self.options.verbose) {
				masterCommand.log({
					prefix: self.toString("executing ")
				});
			}
			async.eachSeries(children, function(child, _done) {
				// We ALWAYS assume that there is a single driving traceId and one way or the other we've got it.
				_.merge(child.command.options, self.options.autoMerge);
				// Many commands will be instances of the parent. We have already logged the parent. Let's avoid redundant and misleading logging.
				if(self.options.verbose && (children.length>1 || children[0].command!==masterCommand)) {
					child.command.log({
						prefix: self.toString("|- executing ")
					});
				}
				// take a snapshot of history and then update it before processing the command (just in case he synchronously returns)
				const _history=(child.command.execute.length>1)
					? history.clone()
					: undefined;
				history.addCommand(child.command, child.parent);
				try {
					self._executeCommand(child.command, _history, function(error) {
						error=child.command.filterError(error);
						if(!error) {
							// Track the result. We don't want to interfere with the command's own management of 'result' so if he doesn't
							// send one then we don't assume anything.
							if(arguments.length>1) {
								child.command.result=arguments[1];
							}
							_done();
						} else {
							const policy=child.command.getErrorPolicy(self.getErrorPolicy());
							error=child.command.error=(error instanceof Error) ? error : new Error(error.toString());
							// if the error is one of ours then let's always associate the command instance with the error.
							if(error instanceof PigError) {
								PigError.annotate(error, {instance: child.command});
							}
							child.command.log({
								error,
								severity: policy
							});
							if(policy===constant.severity.ERROR) {
								// mark the rest of the commands in this hierarchy as being in error so that it cannot get missed
								masterCommand.error=child.parent.error=error;
								// take use out of the exception chain otherwise our try catch runs the risk of catching exceptions thrown by handler
								process.nextTick(_done, error);
							} else {
								_done();
							}
						}
					});
				} catch(error) {
					error=(error instanceof Error) ? error : new Error(error.toString());
					child.command.error=child.parent.error=masterCommand.error=error;
					if(error instanceof PigError) {
						PigError.annotate(error, {instance: child.command});
						child.command.log({
							error,
							message: `known error thrown during ${child.getName()}.execute()`,
							severity: constant.severity.WARN
						});
					} else {
						child.command.log({
							error,
							message: `unknown error thrown during ${child.getName()}.execute()`,
							stack: true
						});
					}
					// We assume that at this level that the exception was that something went awry and is always an error.
					process.nextTick(_done, error);
				}
			}, done);
		}, function(error) {
			callback(error, history);
		});
	}


	/**
	 * Executes this command. His claim to fame is that he processes both promises and callbacks
	 * @param {Command} command
	 * @param {History} history
	 * @param {Function} callback
	 * @private
	 */
	_executeCommand(command, history, callback) {
		const result=command.execute(callback, history);
		if(result instanceof Promise) {
			result
				.then(_.partial(callback, null))
				.catch(callback);
		}
	}

	/**
	 * Expands this command into his children and his children and his children....until we get down to identity an identity set of children
	 * @param {Command} command
	 * @param {History} history
	 * @param {Command} parent is the tippy-top command (in our this._queue). The one we call the "masterCommand"
	 * @param {[{command:Command, parent:Command}]} queue
	 * @returns {[Command]}
	 * @private
	 */
	_expand({command, history, parent=undefined, queue=[]}) {
		let children=command.commands(history);
		parent=parent||command;
		// we support some "mistakes" that are shortcuts
		if(children==null) {
			children=[];
		}
		if(children instanceof Command) {
			children=[children];
		}
		for(let index=0; index<children.length; index++) {
			// we don't allow recursion. It's how we know we should no longer expand a 'this'
			if(children[index]===command) {
				queue.push({
					command: children[index],
					parent: parent
				});
			} else {
				queue=this._expand({
					command: children[index],
					history,
					parent,
					queue
				});
			}
		}
		return queue;
	}
}

/**
 * process execution queue in series
 * @typedef {Queue} SeriesQueue
 */
class SeriesQueue extends Queue {
	/**
	 * Executes commands in this queue in series. It makes sure that all command share the same traceId.
	 * @param {Function} callback will be provided if left undefined
	 */
	execute(callback=undefined) {
		this._execute(async.eachSeries, callback);
	}
}

/**
 * process execution queue in parallel
 * @typedef {Queue} ParallelQueue
 */
class ParallelQueue extends Queue {
	/**
	 * Executes commands in this queue in parallel. It makes sure that all command share the same traceId.
	 * @param {Function} callback will be provided if left undefined
	 */
	execute(callback=undefined) {
		this._execute(async.each, callback);
	}
}

/**
 * Utility method for creating and executing single command execution queues
 * @param {Command} command
 * @param {Function} callback
 * @param {Object} options
 */
function executeCommand(command, callback, options=undefined) {
	const queue=new SeriesQueue(options);
	queue.addCommand(command);
	queue.execute(callback);
}

module.exports={
	executeCommand,
	ParallelQueue,
	SeriesQueue
};
