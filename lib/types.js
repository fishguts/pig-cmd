/**
 * User: curtis
 * Date: 11/14/18
 * Time: 12:21 AM
 * Copyright @2018 by Xraymen Inc.
 *
 * Library of locally used types
 */

/********************* Simple Types *********************/
/**
 * Level of detail that gets logged
 * @typedef {("none"|"min"|"max")} PigLogDetail
 */

/********************* Comples Types *********************/
/**
 * @typedef {Object} PigCliAction
 * @property {string} args - text description of expected args
 * @property {string} desc - text description of the action
 * @property {function(command:Command):function} handler - function that will do the work
 * @property {function(position:Array<string>, options:Object<string, PigCliOption>)} validate
 */

/**
 * @typedef {Object} PigCliOption
 * @property {Object} args
 * @property {number} args.count
 * @property {string|undefined} args.name - description of arg if there is one
 * @property {Object} keys
 * @property {string} keys.short - the single hyphen command line option name
 * @property {string} keys.long - the double hyphen command line option name
 * @property {Boolean|undefined} required - whether option is required or optional (by default)
 */

/**
 * @typedef {Object} PigCliInterface
 * @property {Object<string, PigCliAction>} ACTIONS
 * @property {Array<PigCliOption>} OPTIONS
 * @property {Command} CommandInterface
 */
