/**
 * User: curtis
 * Date: 05/27/18
 * Time: 11:05 PM
 * Copyright @2018 by Xraymen Inc.
 *
 * @module pig-cmd/file
 */

const _=require("lodash");
const async=require("async");
const fs=require("fs-extra");
const path=require("path");
const http=require("pig-core").http;
const {PigError}=require("pig-core").error;
const {Command}=require("./command");


/**
 * Base class for any file command
 * @typedef {Command} FileCommand
 */
class FileCommand extends Command {
	/**
	 * @param {string} path
	 * @param {Object} options
	 */
	constructor(path, options=undefined) {
		super(options);
		this.path=path;
	}
}

/**
 * Reads file contents and stashes in result
 * @typedef {Command} ReadFileCommand
 */
class ReadFileCommand extends Command {
	/**
	 * @param {string} path
	 * @param {string} encoding
	 * @param {string} flags
	 */
	constructor(path, {
		encoding="utf8",
		flags=undefined
	}) {
		super({encoding, flags});
		this.path=path;
	}

	execute(callback) {
		fs.readFile(this.path, _.pick(this.options, ["encoding", "flag"]), function(error, content) {
			if(error) {
				callback(new PigError({error}));
			} else {
				callback(null, content);
			}
		});
	}
}

/**
 * Calls back with a 404 error if path does not exist. Supports additional optional tests.
 * @typedef {FileCommand} PathExistsCommand
 */
class PathExistsCommand extends FileCommand {
	/**
	 * @param {string} path
	 * @param {Object} options
	 * @param {Boolean=undefined} options.testIsFile - assert that is file
	 * @param {Boolean=undefined} options.testIsDirectory - assert that is directory
	 * @param {function(stat:Stats):Boolean} options.test - true if okay and false if fail
	 * @param {string=undefined} options.errorMessage - optional message. Otherwise defaults to statusCode message.
	 */
	constructor(path, options=undefined) {
		super(path, options);
	}

	execute(callback) {
		try {
			this._testFileStat(fs.lstatSync(this.path));
			process.nextTick(callback);
		} catch(error) {
			process.nextTick(callback, new PigError(_.omitBy({
				error: error,
				message: this.options.errorMessage,
				statusCode: _.get(error, "statusCode", http.status.code.NOT_FOUND)
			}, _.isUndefined)));
		}
	}

	/**
	 * Apply optional tests
	 * @param {Stats} stat
	 * @throws {Error}
	 */
	_testFileStat(stat) {
		if(this.options.hasOwnProperty("testIsFile")) {
			if(stat.isFile()!==this.options.testIsFile) {
				throw new PigError({
					details: "resource is not a file",
					statusCode: http.status.code.FORBIDDEN
				});
			}
		} else if(this.options.hasOwnProperty("testIsDirectory")) {
			if(stat.isDirectory()!==this.options.testIsDirectory) {
				throw new PigError({
					details: "resource is not a directory",
					statusCode: http.status.code.FORBIDDEN
				});
			}
		}
		if(this.options.hasOwnProperty("test")) {
			if(!this.options.test(stat)) {
				throw new PigError({
					details: "failed file test",
					statusCode: http.status.code.FORBIDDEN
				});
			}
		}
	}
}

/**
 * Calls back with a 409 (CONFLICT) error if the path DOES exist
 * @typedef {FileCommand} PathNotExistsCommand
 */
class PathNotExistsCommand extends FileCommand {
	execute(callback) {
		if(!fs.pathExistsSync(this.path)) {
			process.nextTick(callback);
		} else {
			process.nextTick(callback, new PigError({
				message: "already exists",
				path: this.path,
				statusCode: http.status.code.CONFLICT
			}));
		}
	}
}

/**
 * Ensures that the target path exists
 * @typedef {FileCommand} EnsurePathCommand
 */
class EnsurePathCommand extends FileCommand {
	/**
	 * @param {string} path
	 * @param {Object} options
	 * @param {Boolean=false} options.parse - parse path
	 */
	constructor(path, options=undefined) {
		super(path, options);
	}

	execute(callback) {
		const directory=this.getOption("parse")
			? path.parse(this.path).dir
			: this.path;
		fs.ensureDir(directory, callback);
	}
}

/**
 * Ensures that the target path does not exist
 * @typedef {FileCommand} EnsureNotPathCommand
 */
class EnsureNotPathCommand extends FileCommand {
	execute(callback) {
		if(fs.pathExistsSync(this.path)) {
			fs.remove(this.path, callback);
		} else {
			process.nextTick(callback);
		}
	}
}

/**
 * Will copy files or paths and assumes that the business is recursive if the source is a directory
 * @typedef {FileCommand} CopyPathCommand
 */
class CopyPathCommand extends FileCommand {
	/**
	 * @param {string} pathFrom
	 * @param {string} pathTo
	 * @param {Object} options
	 * @param {Boolean=true} options.overwrite
	 * @param {Boolean=false} options.errorOnExist
	 */
	constructor(pathFrom, pathTo, options) {
		super(pathTo, Object.assign({
			overwrite: true,
			errorOnExist: false
		}, options));
		this.pathFrom=pathFrom;
		this.pathTo=pathTo;
	}

	execute(callback) {
		const options=_.pick(this.options, ["overwrite", "errorOnExist"]);
		fs.copy(this.pathFrom, this.pathTo, options, callback);
	}
}

/**
 * Deletes the file or directory at the specified path. Will remove directories even if not empty.
 * @typedef {FileCommand} DeletePathCommand
 */
class DeletePathCommand extends FileCommand {
	execute(callback) {
		fs.remove(this.path, callback);
	}
}

/**
 * Moves a path from one location to another. Will create directories along the way as needed.
 * @typedef {FileCommand} MovePathCommand
 */
class MovePathCommand extends FileCommand {
	/**
	 * @param {string} pathFrom
	 * @param {string} pathTo
	 * @param {Object} options
	 * @param {Boolean=true} options.overwrite
	 */
	constructor(pathFrom, pathTo, options=undefined) {
		super(pathFrom, options);
		this.pathTo=pathTo;
	}

	execute(callback) {
		if(this.path===this.pathTo) {
			process.nextTick(callback);
		} else {
			// if the to path is nested in the from path guy and it's a directory then we have to get a little smart
			if(_.startsWith(this.pathTo, this.path)
				&& fs.lstatSync(this.path).isDirectory()) {
				this._moveNested(callback);
			} else {
				fs.move(this.path, this.pathTo, {overwrite: this.getOption("overwrite", false)}, callback);
			}
		}
	}

	/**
	 * Moving a directory to a directory within that directory. Here's the plan:
	 *    1. read the contents of the directory and stash them
	 *    2. build the nested directory
	 *    3. move his contents (except for the target) to the nest
	 * @param {Function} callback
	 * @private
	 */
	_moveNested(callback) {
		const self=this,
			storage={
				files: []
			};
		async.waterfall([
			fs.readdir.bind(fs, self.path),
			function(files, done) {
				storage.files=files.map(function(file) {
					return {
						name: file,
						path: path.join(self.path, file)
					};
				});
				fs.ensureDir(self.pathTo, done);
			},
			function(stats, done) {
				async.eachSeries(storage.files, function(file, _done) {
					// we don't want to move this file if he is the subdirectory we are targetting. Get it? Got it? Good!
					if(_.startsWith(self.pathTo, file.path)) {
						process.nextTick(_done);
					} else {
						fs.move(file.path, path.join(self.pathTo, file.name), _done);
					}
				}, done);
			}
		], callback);
	}
}

module.exports={
	CopyPathCommand,
	EnsurePathCommand,
	EnsureNotPathCommand,
	DeletePathCommand,
	MovePathCommand,
	PathExistsCommand,
	PathNotExistsCommand,
	ReadFileCommand
};
