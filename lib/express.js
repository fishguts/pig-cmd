/**
 * User: curt
 * Date: 11/11/17
 * Time: 4:55 PM
 * Copyright @2017 by Xraymen Inc.
 *
 * @module pig-cmd/express
 */

const bodyParser=require("body-parser");
const express=require("express");
const http=require("http");
const constant=require("pig-core").constant;
const {PigError}=require("pig-core").error;
const log=require("pig-core").log;
const notification=require("pig-core").notification;
const util=require("pig-core").util;
const {Command}=require("./command");
const configuration=require("./configuration");

/**
 * A class for setting up a single express server.
 * Note: you should have <a href="./configuration.js">configured</a> the lib by the time this guy is used if you are relying on defaults.
 * @typedef {Command} ExpressServerCommand
 */
class ExpressServerCommand extends Command {
	/**
	 * build him
	 * @param {string} nodenv
	 * @param {string} limit
	 * @param {string} name
	 * @param {number} port
	 * @param {string} staticPath - optional path from which static content is served
	 * @param {Object} options - additional options supported by base class
	 */
	constructor({
		nodenv=configuration.nodenv,
		limit="100kb",
		name=configuration.name,
		port=configuration.port,
		staticPath=null,
		...options
	}={}) {
		super(options);
		this._nodenv=nodenv;
		this._name=name;
		this._port=port;
		this.app=express();
		this.router=new express.Router();
		this.server=null;
		this.app.use("/", this.router);
		this.router.use(bodyParser.json({limit}));
		if(staticPath) {
			this.router.use(express.static(staticPath));
		}
	}

	/**
	 * start up the server
	 * @param {Function} callback
	 */
	execute(callback) {
		this.server=http.createServer(this.app);
		this.app.set("port", this._port);
		this.server.on("error", (error)=>{
			callback(new PigError({
				error,
				message: `attempt to listen on port=${this._port} failed`
			}));
		});
		this.server.listen(this._port, ()=>{
			log.info(this.toString(`"${this._name}" server listening on port=${this._port}, NODE_ENV=${this._nodenv}`));
			this._setupInterruptHandling();
			callback();
		});
	}

	/**** Private Interface ****/
	/**
	 * Sets up both interrupt and unhandled-exception handling.  In the event of either he does
	 * his best to log what happened and then to gracefully shutdown and give our environment a
	 * chance to process the shutdown.
	 * @private
	 */
	_setupInterruptHandling() {
		const self=this;

		/**
		 * Shuts her down and exists with <param>code</param>
		 * @param {number} code
		 * @param {string} prefix
		 * @param {number} seconds
		 */
		function shutdown({
			code=0,
			prefix="",
			seconds=5
		}={}) {
			// disable our client interface
			util.try(self.server.close.bind(self));
			notification.emit(constant.event.SHUTDOWN_START);
			log.warn(self.toString(`${prefix}shutting "${self._name}" down in ${seconds} seconds`));
			setTimeout(()=>process.exit(code), seconds*1000);
		}

		process.addListener("uncaughtException", function(error) {
			log.error(self.toString({
				message: "uncaught exception",
				error: error,
				stack: true
			}));
			shutdown({
				code: 1,
				prefix: "uncaught exception - "
			});
		});

		[
			{signal: "SIGINT", seconds: 5, note: "graceful"},
			{signal: "SIGQUIT", seconds: 0, note: "immediate/test"},
			{signal: "SIGTERM", seconds: 5, note: "graceful"}
		].forEach((spec)=>{
			process.addListener(spec.signal, function(sigstr, sigint) {
				shutdown({
					prefix: `${sigstr} (${sigint}) received - `,
					seconds: spec.seconds
				});
			});
		});

	}
}

module.exports={
	ExpressServerCommand
};
