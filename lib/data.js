/**
 * User: curtis
 * Date: 6/3/18
 * Time: 12:46 AM
 * Copyright @2018 by Xraymen Inc.
 *
 * @module pig-cmd/data
 */

const _=require("lodash");
const {Command}=require("./command");

/**
 * Allows one to both filter and map data retrieved sometime before this command is executed.
 * @typedef {Command} FilterInputCommand
 */
class FilterInputCommand extends Command {
	/**
	 * @param {Command|class} inputCommand - command or type to use as source of input data
	 * @param {[*]} inputData -  explicitly set input data. Overrides all other data.
	 * @param {function(data):boolean} filter
	 * @param {function(data):Object} map
	 * @param {Object} options
	 */
	constructor({
		inputCommand=undefined,
		inputData=undefined,
		filter=(data)=>data,
		map=(data)=>data,
		...options
	}) {
		super({inputCommand, inputData, ...options});
		this.filter=filter;
		this.map=map;
	}

	execute(callback, history) {
		const input=this._getInputData(history);
		if(input) {
			if(_.isArray(input)) {
				process.nextTick(callback, null, _(input)
					.filter(this.filter)
					.map(this.map)
					.value());
			} else {
				process.nextTick(callback, null, _([input])
					.filter(this.filter)
					.map(this.map)
					.value()[0]);
			}
		}
	}
}

module.exports={
	FilterInputCommand
};
