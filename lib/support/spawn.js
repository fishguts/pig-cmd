/**
 * User: curt
 * Date: 12/01/2017
 * Time: 5:45 PM
 *
 * @module pig-cmd/support/spawn
 *
 * Starts up a nodejs server
 */

/* eslint-disable no-console */

const _=require("lodash");
const child_process=require("child_process");
const path=require("path");
const {PigError}=require("pig-core").error;
const configuration=require("../configuration");

const storage={
	/**
	 * @type {Object<string, NodeServer>}
	 */
	singletons: {}
};


/**
 * Spins up a project server for testing
 */
class NodeServer {
	/**
	 * @param {string} logPrefix
	 * @param {string} nodenv
	 * @param {Array<string>} serverArgs
	 * @param {string} serverPath - relative to serverRoot
	 * @param {string} serverRoot - will be the spawned process's CWD
	 * @param {boolean} verbose
	 */
	constructor({
		logPrefix="[spawned-server]",
		nodenv=configuration.nodenv,
		serverArgs=[],
		serverPath,
		serverRoot,
		verbose=configuration.verbose
	}) {
		this._logPrefix=logPrefix;
		this._nodenv=nodenv;
		this._serverArgs=serverArgs;
		this._serverPath=serverPath;
		this._serverRoot=serverRoot;
		this._verbose=verbose;
		/**
		 * @type {ChildProcess}
		 * @private
		 */
		this._process=null;
	}

	/**
	 * Gets singleton instance. Singleton instances are associated with their full path.
	 * @param {string} logPrefix
	 * @param {string} nodenv
	 * @param {Array<string>} serverArgs
	 * @param {string} serverPath - relative to serverRoot
	 * @param {string} serverRoot
	 * @param {boolean} verbose
	 * @return {NodeServer}
	 */
	static getSingleton({
		logPrefix="[spawned-server]",
		nodenv=configuration.nodenv,
		serverArgs=[],
		serverPath,
		serverRoot,
		verbose=configuration.verbose
	}) {
		const key=path.join(serverRoot, serverPath),
			server=storage.singletons[key];
		if(!server) {
			storage.singletons[key]=new NodeServer({logPrefix, nodenv, serverArgs, serverPath, serverRoot, verbose});
		} else {
			// our singleton model breaks down with option differences. To get around this we simply inject this
			// guys options and make sure we react dynamically to option properties
			server._logPrefix=logPrefix;
			server._nodenv=nodenv;
			server._serverArgs=serverArgs;
			server._serverPath=serverPath;
			server._serverRoot=serverRoot;
			server._verbose=verbose;
		}
		return storage.singletons[key];
	}

	/**
	 * Is this spawned process currently running?
	 * @returns {boolean}
	 */
	isRunning() {
		return (this._process && !this._process.killed);
	}

	/**
	 * Starts up the server if it is not already started or has been stopped
	 * @param {Function} callback
	 */
	start(callback) {
		callback=_.once(callback);
		if(this._process && !this._process.killed) {
			// we assume that our tests are synchronous and that nothing will get this far without a successful startup
			process.nextTick(callback);
		} else {
			try {
				let processFullyStarted=false;
				const logVerbose=(this._verbose)
					? (text)=>console.debug(`${this._logPrefix} ${text}`)
					: _.noop;

				this._process=this._spawn();
				this._process.stdout.on("data", (data)=>{
					data=data.toString();
					logVerbose(`${_.trim(data, "\n")}`);
					if(/listening on port=\d+/.test(data)) {
						processFullyStarted=true;
						callback();
					}
				});
				this._process.stderr.on("data", (data)=>{
					data=data.toString();
					// winston puts DEBUG output on stderr.  If it looks like debug data then ignore it
					if(data.indexOf("DEBUG:")> -1) {
						logVerbose(`${_.trim(data, "\n")}`);
					} else {
						console.error(`${this._logPrefix} ${_.trim(data, "\n")}`);
						// if on startup we experience an error we consider it to be fatal.
						if(processFullyStarted===false) {
							this._process.kill("SIGKILL");
							callback(new Error(data));
						}
					}
				});
				this._process.on("exit", (code, signal)=>{
					// if we haven't been killed then send out a warning 'cause he died jim
					if(this._process) {
						console.warn(`${this._logPrefix} exited code=${code}, signal=${signal}`);
					}
				});
			} catch(error) {
				process.nextTick(callback, new PigError({
					error,
					instance: this,
					message: `attempt to spawn ${path.join(this._serverRoot, this._serverPath)} failed`
				}));
			}
		}
	}

	/**
	 * Kill the server if it is running and calls you back after the funeral
	 * @param {Function} callback
	 */
	stop(callback) {
		if(!this.isRunning()) {
			process.nextTick(callback);
			this._process=null;
		} else {
			this._process.on("exit", _.ary(callback, 1));
			// SIGQUIT (3) may be handled and allows us to react and to which we shutdown immediately
			process.kill(this._process.pid, "SIGQUIT");
			this._process=null;
		}
	}

	/********************* Private Interface *********************/
	/**
	 * @returns {ChildProcess}
	 * @throws {Error}
	 * @private
	 */
	_spawn() {
		const absServerPath=path.join(this._serverRoot, this._serverPath);
		return child_process.spawn("node", [absServerPath].concat(this._serverArgs), {
			cwd: this._serverRoot,
			shell: true,
			env: {NODE_ENV: this._nodenv}
		});
	}
}

exports.NodeServer=NodeServer;
