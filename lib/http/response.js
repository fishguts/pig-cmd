/**
 * User: curtis
 * Date: 05/27/18
 * Time: 11:29 PM
 * Copyright @2018 by Xraymen Inc.
 *
 * @module pig-cmd/http/response
 */

const _=require("lodash");
const async=require("async");
const fs=require("fs-extra");
const mime=require("mime");
const {Stream}=require("stream");
const assert=require("pig-core").assert;
const http=require("pig-core").http;
const util=require("pig-core").util;
const {PigError}=require("pig-core").error;
const command=require("../command");
const configuration=require("../configuration");

/**
 * @typedef {{toString:function():string}} RequestURL
 */

/**
 * Base route class for all of our response commands
 * @abstract
 */
class ResponseCommand extends command.Command {
	/**
	 * @param {ServerResponse} res
	 * @param {Object} options
	 * @param {string} options.contentType - will automatically set if the send type is explicit as in res.json
	 *    or will automatically derive the type from the file extension if the request is a send file contents request.
	 * @param {Error} options.error - respond with error
	 * @param {Command} options.inputCommand - get response data from this command
	 * @param {*} options.inputData - this is the response data
	 * @param {RequestURL} options.inputFilePath - get response data from this file
	 * @param {Boolean} options.inputLastCommand - use last command's result as response
	 * @param {Stream} options.inputStream - stream response from this stream
	 * @param {Boolean} options.json - whether to treat response data as JSON by default
	 * @param {number} options.statusCode
	 */
	constructor(res, options=undefined) {
		assert.notEqual(res, null);
		super(options);
		this.res=res;
	}

	/**
	 * Sets up defaults and then calls _execute
	 * @param {Function} callback
	 * @param {History} history
	 */
	execute(callback, history) {
		this.res.status=(this.getOption("statusCode", http.status.code.OK));
		if(this.options.contentType) {
			this.res.header("Content-Type", this.options.contentType);
		}
		this._execute(callback, history);
	}

	/**** Protected Interface ****/
	/**
	 * Sets up defaults and then calls _execute
	 * @param {Function} callback
	 * @param {History} history
	 * @abstract
	 */
	_execute(callback, history) {
		throw new Error("implement");
	}

	/**
	 * Responds with error status and details
	 * @param {Error} error
	 * @protected
	 */
	_sendError(error) {
		const statusCode=_.get(error, "statusCode", http.status.code.INTERNAL_SERVER_ERROR);
		this.res.statusCode=statusCode;
		// we won't share inner secrets with our upper environments as this will be public facing
		if(configuration.isLowerEnv) {
			this.res.json(Object.assign({
				// message: this must be a get attribute on message. It does not get assigned.
				"message": error.message,
				"nodenv": configuration.nodenv,
				"debug": "Additional properties only shared in lower environments"
			}, util.objectToData(error)));
		} else if(error.message) {
			this.res.json(Object.assign({
				"message": error.message,
				"details": error.details
			}));
		} else {
			this.res.sendStatus(statusCode);
		}
	}

	/**
	 * Gets response body content.
	 * Assumes one of our supported means of input (see options documentation for the command of interest)
	 * @param {History} history
	 * @param {Function} callback
	 * @protected
	 */
	_getResponseBody(history, callback) {
		if(this.options.hasOwnProperty("inputFilePath")) {
			this._getResponseBodyFromFile(this.options.inputFilePath, callback);
		} else {
			let input;
			if(this.options.hasOwnProperty("inputStream")) {
				input=this.options.inputStream;
			} else {
				input=this._getInputData(history);
			}
			if(input instanceof Stream) {
				// We can stream Streams directly. Our caller make sense of it.
				process.nextTick(callback, null, input);
			} else {
				// if the response type is JSON then we are going to assume that it's an object of some sort that will get serialized as json.
				if(this.options.json) {
					process.nextTick(callback, null, input);
				} else {
					process.nextTick(callback, new PigError({
						message: `unknown input-type=${input} for contentType=${this.options.contentType}`
					}));
				}
			}
		}
	}

	/**
	 * @param {RequestURL} filePath
	 * @param {Function} callback
	 * @protected
	 */
	_getResponseBodyFromFile(filePath, callback) {
		// unless he knows better we will assume the content type of the file extension
		if(!this.options.hasOwnProperty("contentType")) {
			this.res.type(mime.getType(filePath));
		}
		if(this.options.json) {
			fs.readJSON(filePath.toString(), callback);
		} else {
			fs.readFile(filePath.toString(), callback);
		}
	}
}


/**
 * A very generic send command who will consider all input options and try to do the right thing.
 * See options at <code>ResponseCommand</code> for more info on what is supported.
 */
class HttpSendCommand extends ResponseCommand {
	_execute(callback, history) {
		const self=this;
		async.waterfall([
			self._getResponseBody.bind(self, history),
			function(body) {
				if(body instanceof Stream) {
					body.pipe(self.res);
				} else {
					self.res.send(body);
				}
				process.nextTick(callback);
			}
		], self._sendError.bind(self));
	}
}


/**
 * Sends a statusCode back.
 * - Assumes 200 if no error.
 * - Assumes 500 if there is an error.
 */
class HttpSendStatusCommand extends ResponseCommand {
	_execute(callback) {
		if(!_.has(this.options, "error")) {
			this.res.sendStatus(this.getOption("statusCode", http.status.code.OK));
		} else {
			this._sendError(this.options.error);
		}
		process.nextTick(callback);
	}
}


/**
 * Sends json that should be discoverable via our command input mechanism. See Command._getInputData
 */
class HttpSendJsonCommand extends ResponseCommand {
	/**
	 * @param {ServerResponse} res
	 * @param {*} inputData
	 * @param {Command} inputCommand
	 * @param {Boolean} inputLastCommand
	 * @param {RequestURL} inputFilePath
	 * @param {ReadableStream} inputStream
	 * @param {Object} options
	 */
	constructor(res, {
		inputCommand=undefined,
		inputData=undefined,
		inputFilePath=undefined,
		inputLastCommand=false,
		inputStream=false,
		...options
	}) {
		assert.ok(inputCommand || inputData || inputFilePath || inputStream || inputLastCommand);
		super(res, {
			inputData,
			inputCommand,
			inputLastCommand,
			inputFilePath,
			json: true,
			...options
		});
	}

	_execute(callback, history) {
		const self=this;
		async.waterfall([
			self._getResponseBody.bind(self, history),
			function(json) {
				self.res.json(json);
				process.nextTick(callback);
			}
		], self._sendError.bind(self));
	}
}


/**
 * Sends a file located at a inputFilePath to the response endpoint.
 * If no contentType is specified in options then it will be derived from the inputFilePath's file extension
 */
class HttpSendFileComand extends ResponseCommand {
	/**
	 * @param {ServerResponse} res
	 * @param {RequestURL} inputFilePath
	 * @param {Object} options
	 */
	constructor(res, inputFilePath, options=undefined) {
		super(res, {
			inputFilePath,
			...options
		});
	}

	_execute(callback) {
		this.res.sendFile(this.options.inputFilePath.toString(), callback);
	}
}

module.exports={
	HttpSendCommand,
	HttpSendFileComand,
	HttpSendJsonCommand,
	HttpSendStatusCommand
};
