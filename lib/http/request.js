/**
 * User: curtis
 * Date: 05/27/18
 * Time: 6:51 PM
 * Copyright @2018 by Xraymen Inc.
 *
 * @module pig-cmd/http/request
 */

const _=require("lodash");
const fs=require("fs-extra");
const request=require("request");
const assert=require("pig-core").assert;
const {PigError}=require("pig-core").error;
const http=require("pig-core").http;
const {Command}=require("../command");

const SUPPORTED_OPTIONS=[
	"auth",
	"body",
	"contentType",
	"encoding",
	"formData",
	"json",
	"method",
	"multipart",
	"oauth",
	"qs",
	"timeout"
];

/**
 * Base class for all request handlers. It composes <code>request</code> - https://github.com/request/request/
 * It's designed to have most core functionality that we will draw upon. Exceptions are PUT and POST.
 * They are tasked with their body duties.
 * @abstract we say he is abstract because we don't expose him directly. Clients should use one of the derivations.
 * @typedef {Command} HttpRequestCommand
 */
class HttpRequestCommand extends Command {
	/**
	 * Most of our settings are passed-through to <a href="https://github.com/request/request">request</a>. We do assume control the contentType
	 * and response parsing if the contentType is set to "application/json".
	 * @param {Url|string} url
	 * @param {Object} options see SUPPORTED_OPTIONS for additional guys that get forwarded to `request`
	 * @param {string=undefined} options.allows
	 * @param {Object|Buffer|string|ReadStream} options.body - If json is true or contentType="application/json", then body must be a JSON-serializable object.
	 * @param {Object} options.json - same as specifying body as an Object. contentType should be allowed to default to "application/json"
	 * @param {string="application/json"} options.contentType
	 * @param {Object=undefined} options.headers
	 * @param {string=undefined} options.method - defaults at the request level to "application/json"
	 * @param {Function} options.resultFilter - function that takes the incomming result and transforms it into whatever the command result should be.  Defaults to incomming.body
	 * @param {Object} options.qs - object containing querystring values to be appended to the uri
	 */
	constructor(url, options=undefined) {
		assert.notEqual(url, null);
		super(Object.assign({
			resultFilter: (incomming)=>incomming.body,
			contentType: http.content.type.JSON
		}, options));
		this.url=url;
		if(!this.options.hasOwnProperty("json")) {
			if(this.options.contentType===http.content.type.JSON) {
				this.options.json=true;
			}
		} else {
			assert.equal(this.options.contentType, http.content.type.JSON);
		}
	}

	execute(callback) {
		const self=this,
			uri=this.url.toString(),
			options=_.assign({
				method: http.method.GET,
				uri: uri,
				headers: this._getHeaders()
			}, _.pick(this.options, SUPPORTED_OPTIONS));
		request(options, function(error, incomming) {
			if(error) {
				callback(new PigError({
					error,
					url: uri
				}));
			} else if(incomming.statusCode<400) {
				callback(null, self.options.resultFilter(incomming));
			} else {
				const errorProperties=_.isPlainObject(incomming.body)
					? incomming.body
					: {};
				errorProperties.url=uri;
				errorProperties.statusCode=incomming.statusCode;
				errorProperties.statusMessage=incomming.statusMessage;
				if(!errorProperties.message) {
					errorProperties.message=`failed: ${incomming.statusCode} - ${incomming.statusMessage}`;
				}
				// we are sort of assuming that the details are our own. But
				callback(new PigError(errorProperties));
			}
		});
	}

	/**** Protected Interface ****/
	/**
	 * Gather what should be stuffed in the options.header object
	 * @param {Object} mixin
	 * @returns {Object}
	 * @private
	 */
	_getHeaders(mixin={}) {
		const headers=(this.options.headers)
			? _.clone(this.options.headers)
			: {};
		if(mixin) {
			Object.assign(headers, mixin);
		}
		if(this.options.traceId) {
			headers["TraceId"]=this.options.traceId;
		}
		return headers;
	}
}


/**
 * GET method (which is the default)
 * @typedef {HttpGetRequestCommand} HttpRequestCommand
 */
class HttpGetRequestCommand extends HttpRequestCommand {
	constructor(url, options=undefined) {
		super(url, Object.assign({
			method: http.method.GET
		}, options));
	}
}


/**
 * A base class for the variety of things that may include a body
 * @typedef {HttpRequestCommand} HttpBodyRequestCommand
 */
class HttpBodyRequestCommand extends HttpRequestCommand {
	/**
	 * @param {Url|string} url
	 * @param {Object} options
	 * @param {Object} options.json
	 * @param {Object|string|Stream|Path|undefined} options.body
	 * @param {string} options.@param parts.path - of file to upload
	 */
	constructor(url, options={}) {
		if(options.path) {
			options=_.omit(options, "path");
			options.body=fs.createReadStream(options.path);
		} else if(_.isPlainObject(options.body)) {
			const json=options.body;
			options=_.omit(options, "body");
			options.json=json;
		}
		super(url, options);
	}
}

/**
 * @typedef {HttpBodyRequestCommand} HttpPutRequestCommand
 */
class HttpPutRequestCommand extends HttpBodyRequestCommand {
	/**
	 * See HttpBodyRequestCommand for details
	 * @param {Url|string} url
	 * @param {Object} options
	 */
	constructor(url, options={}) {
		super(url, Object.assign({
			method: http.method.PUT
		}, options));
	}
}

/**
 * @typedef {HttpBodyRequestCommand} HttpPostRequestCommand
 */
class HttpPostRequestCommand extends HttpBodyRequestCommand {
	/**
	 * See HttpBodyRequestCommand for details
	 * @param {Url|string} url
	 * @param {Object} options
	 */
	constructor(url, options={}) {
		super(url, Object.assign({
			method: http.method.POST
		}, options));
	}
}


/**
 * assumes DELETE method
 * @typedef {HttpRequestCommand} HttpDeleteRequestCommand
 */
class HttpDeleteRequestCommand extends HttpRequestCommand {
	/**
	 * @param {Url|string} url
	 * @param {Object} options
	 */
	constructor(url, options=undefined) {
		super(url, Object.assign({
			method: http.method.DELETE
		}, options));
	}
}


/**
 * Encodes a multipart request.  Defaults to a POST request but may be overridden
 * @typedef {HttpRequestCommand} HttpMultipartRequestCommand
 */
class HttpMultipartRequestCommand extends HttpRequestCommand {
	/**
	 * @param {Url|string} url
	 * @param {Object|Array<Object<string, string|number|Object|Buffer|Stream>>} parts - keys will be the multipart key and the values will be the multipart data.
	 * @param {Object} options
	 * @param {Object} options.body
	 */
	constructor(url, parts, options=undefined) {
		const defaults={
			method: http.method.POST
		};
		// note: request will take care of the content-type. We want to override the application/json default
		if(_.isArray(parts)) {
			defaults.multipart=parts;
			defaults.contentType=http.content.type.multi.RELATED;
		} else {
			// we've added a little convenience by assuming that objects should be converted to strings 'cause request doesn't handle them
			defaults.formData=_.mapValues(parts, function(value) {
				return _.isPlainObject(value)
					? JSON.stringify(value)
					: value;
			});
			defaults.contentType=http.content.type.multi.FORM;
		}
		super(url, Object.assign(defaults, options));
	}
}

module.exports={
	HttpGetRequestCommand,
	HttpGetJsonRequestCommand: HttpGetRequestCommand,
	HttpPutRequestCommand,
	HttpPostRequestCommand,
	HttpDeleteRequestCommand,
	HttpMultipartRequestCommand
};
