/**
 * User: curtis
 * Date: 05/27/18
 * Time: 12:00 PM
 * Copyright @2018 by Xraymen Inc.
 *
 * @module pig-cmd/command
 */

const _=require("lodash");
const assert=require("pig-core").assert;
const format=require("pig-core").format;
const log=require("pig-core").log;
const util=require("pig-core").util;
const constant=require("pig-core").constant;
const configuration=require("./configuration");

/**
 * Base class for all commands
 * @abstract
 */
class Command {
	/**
	 * Construct instance
	 * @param {function(error:Error):Boolean} errorFilter - allows errors to be filtered and to selectively eliminate errors from being treated as failures.
	 * @param {string} errorPolicy - how to treat errors. Will default to error
	 * @param {Command|class} inputCommand - command or type to use as source of input data
	 * @param {*} inputData -  explicitly set input data. Overrides all other data.
	 * @param {Boolean} inputLastCommand - use the last command as input. You are better off relying on one of the other input methods.
	 * @param {{error:PigLogDetail, other:PigLogDetail}} logLevel - controls level of detail that gets looged.
	 * @param {string} traceId - (QUEUE-PARAM) trace id that flows downstream
	 * @param {Boolean} verbose - (QUEUE-PARAM) we let the queue auto-merge this setting.
	 * @param {Object} options
	 */
	constructor({
		errorFilter=undefined,
		errorPolicy=undefined,
		inputCommand=undefined,
		inputData=undefined,
		inputLastCommand=false,
		logLevel={
			error: "max",
			other: "max"
		},
		traceId=undefined,
		verbose=configuration.verbose,
		...options
	}={}) {
		// note: we clean options because we encourage our derived instances to use default params, some of which will
		// very likely be undefined. For the purpose of "hasOwnProperty" test we don't want to include these fellas in this.options.
		this.options=_({
			errorFilter,
			errorPolicy,
			inputCommand,
			inputData,
			inputLastCommand,
			logLevel,
			traceId,
			verbose
		})
			.merge(options)
			.omitBy(_.isUndefined)
			.value();
		// these guys are reserved for and processed by command.queue in which he will stash result (if there is one)
		this.result=null;
		this.error=null;
	}

	/**** Public Interface ****/
	/**
	 * Get optional value for specified optional property
	 * @param {string} property
	 * @param {*} dfault
	 * @returns {*}
	 */
	getOption(property, dfault=undefined) {
		return _.get(this.options, property, dfault);
	}

	/**
	 * Sets value for specified property. undefined will delete it
	 * @param {string} property
	 * @param {*} value
	 */
	setOption(property, value=undefined) {
		if(value!==undefined) {
			this.options=_.set(this.options, property, value);
		} else {
			delete this.options[property];
		}
	}

	/**
	 * Gets the very general error handing policy of this command. Allows commands to translate an error to another serverity.
	 * See filterError for more precise handling of errors.
	 * @param {string} dfault
	 * @returns {string}
	 */
	getErrorPolicy(dfault=constant.severity.ERROR) {
		return this.options.errorPolicy || dfault;
	}

	/**
	 * Returns this objects name.  It is designed to allow derived classes to ornament the name in special cases.
	 * One such case might be prepending the name with the module name where
	 * duplicate class names exist
	 * @param {Boolean} presentation - "presentation" as in presented in logs
	 * @returns {string}
	 */
	getName({
		presentation=true
	}={}) {
		return this.constructor.name;
	}

	/**
	 * Applies an error filter if one was specified in this.options. By default it returns `error`
	 * @param {Error} error
	 * @returns {Error|null}
	 */
	filterError(error) {
		return (!error || !this.options.errorFilter)
			? error
			: this.options.errorFilter(error);
	}

	/**** Abstract/Virtual interface ****/
	/* eslint-disable valid-jsdoc */
	/**
	 * Executes this command. He supports both callbacks and Promises.  If you return a Promise then it will be
	 * monitored for then and catch callbacks.  Otherwise you must callback <code>callback</code>
	 * Note: This guy is abstract and will throw an exception if called at this level.
	 * @param {Function} callback
	 * @param {History} history previously run commands. All will have been processed if queue is run in series
	 * @returns {undefined|Promise}
	 */
	execute(callback, history) {
		throw new Error("abstract");
	}

	/**
	 * This allows a command to expand himself into one or more commands at runtime. By default it returns 'this'.
	 * It is called when the command is processed in the queue
	 * @param {History} history - history ONLY includes commands that have been executed!!
	 * @returns {[Command]}
	 * @override
	 */
	commands(history) {
		return [this];
	}

	/**
	 * A convienice method for logging command instance message. It uses <code>toString</code> to annotate this message.  And
	 * applies our own filtering using <code>options.logLevel</code>. If this guy doesn't suite your needs then you may always
	 * log directly to the log, but do process your message with <code>toString</code>
	 * @param {Error} error - transforms the error into text
	 * @param {Boolean} includeInstanceData - whether to include command data or not. By default it will follow the logLevel.
	 * @param {string} message - should always be included except when <param>error</param> is included. Then it's optional.
	 * @param {Boolean} metadata - optional metadata to append or replace the commands own metadata
	 * @param {string} method - optionally specify the method. Will be documented with the class heading
	 * @param {string} prefix - see <code>toString</code>
	 * @param {PigSeverity} severity - defaults to "error" or "debug" depending on state of <param>error</param>
	 * @param {Boolean} stack - whether to include a stack dump
	 * @param {string} suffix - see <code>toString</code>
	 * @param {Boolean} verbose - only log in verbose mode
	 */
	log({
		error=undefined,
		includeInstanceData=undefined,
		message=undefined,
		metadata=undefined,
		method=undefined,
		prefix=undefined,
		severity="smart",
		stack=false,
		suffix=undefined,
		verbose=false
	}) {
		if(severity==="smart") {
			severity=(error)
				? constant.severity.ERROR
				: constant.severity.DEBUG;
		}
		let logDegree=(constant.severity.trips(severity, constant.severity.WARN))
			? this.options.logLevel.error
			: this.options.logLevel.other;
		// override whatever logDegree is if our caller has other ideas
		if(includeInstanceData===true || metadata) {
			logDegree="max";
		} else if(includeInstanceData===false) {
			logDegree="min";
		}
		// 1. see if we are filtering these messages out?
		if(logDegree!=="none") {
			// 2. second level of filtering - do they only want to log if <code>this.options.verbose</code> is set?
			if(!verbose || this.options.verbose) {
				// We are logging. Now it's just a matter of how much data we want to include in the entry.
				const text=this.toString({
					error,
					message,
					method,
					prefix,
					stack,
					suffix
				});
				if(logDegree!=="max") {
					log[severity](text);
				} else {
					metadata=(includeInstanceData!==false)
						? Object.assign(util.objectToData(this), metadata)
						: Object.assign({}, metadata);
					if(error) {
						metadata=Object.assign(metadata, {
							error: util.objectToData(error, {depth: 3})
						});
						log[severity](text, {meta: metadata});
					} else {
						log[severity](text, {meta: metadata});
					}
				}
			}
		}
	}

	/**
	 * Simple little bugger that prefixes message with class name and returns it with the optional message
	 * @param {string|Object} options
	 * @param {Error=undefined} options.error
	 * @param {string=""} options.method
	 * @param {string=""} options.message
	 * @param {string=""} options.prefix
	 * @param {string=""} options.suffix
	 * @param {Boolean=false} options.stack
	 * @returns {string}
	 */
	toString(options="") {
		const name=this.getName();
		if(_.isString(options)) {
			return (options.length>0) ? `${name}: ${options}` : name;
		} else {
			let text=`${options.prefix||""}${name}`;
			if(options.method) {
				if(options.method.indexOf("(")<0) {
					text+=`.${options.method}()`;
				} else {
					text+=`.${options.method}`;
				}
			}
			if(options.message) {
				text+=`: ${options.message}`;
			}
			if(options.suffix) {
				text+=(options.suffix || "");
			}
			if(options.error) {
				const errorText=format.errorToString(options.error, {
					instance: false,
					stack: Boolean(_.get(options, "stack"))
				});
				if(options.message) {
					text+=` - ${errorText}`;
				} else {
					text+=`: ${errorText}`;
				}
			}
			return text;
		}
	}

	/**** Protected Interface ****/
	/**
	 * Gets this command's input as per how we are configured in options
	 * @param {History} history
	 * @returns {*}
	 * @protected
	 */
	_getInputData(history) {
		if(this.options.hasOwnProperty("inputData")) {
			return this.options.inputData;
		} else if(this.options.inputCommand) {
			return (this.options.inputCommand instanceof Command)
				? this.options.inputCommand.result
				: history.lastOfType(this.options.inputCommand).result;
		} else {
			assert.toLog(this.options.inputLastCommand);
			return history.last();
		}
	}
}

/**
 * A generic wrapper around an asynchronous function. Includes exception handling for synchronous processing
 * @typedef {Command} AsyncCommand
 */
class AsyncCommand extends Command {
	/**
	 * Create instance
	 * @param {Function} fnction async function
	 * @param {Object} options
	 */
	constructor(fnction, options=undefined) {
		super(options);
		this.function=fnction;
	}
	execute(callback) {
		try {
			this.function(callback);
		} catch(error) {
			process.nextTick(callback, error);
		}
	}
}

/**
 * Adapts a synchronous command with try catch handling into an asynchronous call. Includes exception handling for the function call.
 * @typedef {Command} SyncCommand
 */
class SyncCommand extends Command {
	/**
	 * Create instance
	 * @param {Function} fnction async function
	 * @param {Object} options
	 */
	constructor(fnction, options=undefined) {
		super(options);
		this.function=fnction;
	}
	execute(callback) {
		try {
			this.function();
			process.nextTick(callback);
		} catch(error) {
			process.nextTick(callback, error);
		}
	}
}

/**
 * Replaces <code>execute</code> with <param>fnction</code>
 * @typedef {Command} HookCommand
 */
class HookCommand extends Command {
	/**
	 * Create instance
	 * @param {function(callback, History)} fnction - becomes <code>this.execute</code>
	 * @param {Object} options
	 */
	constructor(fnction, options=undefined) {
		super(options);
		this.execute=fnction;
	}
}

/**
 * Command history with some methods to help one find a command in history that they may be looking for.
 * 12/22/2017 - I was going to include searching by parent type. But it could be misleading. A command
 * may be used both as a parent and a child.  See if there is a need before jumping in.
 */
class History {
	/**
	 * Constructor
	 * @param {Queue} queue
	 * @param {Array} list
	 */
	constructor(queue, list=[]) {
		this._queue=queue;
		this._list=list;
	}

	/**
	 * Create a clone of `this` guy
	 * @returns {History}
	 */
	clone() {
		return new History(this._queue, this._list.slice());
	}

	/**
	 * Return queue associated with this history
	 * @returns {Queue}
	 */
	get queue() {
		return this._queue;
	}

	/**
	 * By default it gets the last child command in the stack. If you want to ensure the type then include it.
	 * @param {class} type
	 * @returns {Command|null}
	 */
	last(type=undefined) {
		if(this._list.length>0) {
			const last=this._list[this._list.length-1];
			if(type===undefined || last.child instanceof type) {
				return last.child;
			}
		}
		return null;
	}

	/**
	 * Searches forward for a child or parent of type
	 * @param {class} type
	 * @returns {Command|null}
	 */
	firstOfType(type) {
		for(let index=0; index<this._list.length; index++) {
			const item=this._list[index];
			if(item.child instanceof type) {
				return item.child;
			}
		}
		return null;
	}
	/**
	 * Searches backward for a child or parent of type
	 * @param {class} type
	 * @returns {Command|null}
	 */
	lastOfType(type) {
		for(let index=this._list.length-1; index>=0; index--) {
			const item=this._list[index];
			if(item.child instanceof type) {
				return item.child;
			}
		}
		return null;
	}

	/**
	 * Add command to history
	 * @param {Command} child
	 * @param {Command} parent
	 */
	addCommand(child, parent) {
		this._list.push({
			child: child,
			parent: parent
		});
	}
}

module.exports={
	Command,
	History,
	AsyncCommand,
	SyncCommand,
	HookCommand
};
