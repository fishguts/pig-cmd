/**
 * User: curtis
 * Date: 11/14/18
 * Time: 7:21 PM
 * Copyright @2018 by Xraymen Inc.
 *
 * @module pig-cmd/index
 */

const storage={
	cli: null,
	command: null,
	configuration: null,
	data: null,
	express: null,
	file: null,
	http: {
		request: null,
		response: null
	},
	queue: null,
	route: {
		factory: null,
		Route: null
	},
	support: {
		spawn: null
	}
};

module.exports={
	/**
	 * @type {module:pig-cmd/cli}
	 */
	get cli() {
		return storage.cli || (storage.cli=require("./cli"));
	},
	/**
	 * @type {module:pig-cmd/command}
	 */
	get command() {
		return storage.command || (storage.command=require("./command"));
	},
	/**
	 * @type {module:pig-cmd/configuration}
	 */
	get configuration() {
		return storage.configuration || (storage.configuration=require("./configuration"));
	},
	/**
	 * @type {module:pig-cmd/data}
	 */
	get data() {
		return storage.data || (storage.data=require("./data"));
	},
	/**
	 * @type {module:pig-cmd/express}
	 */
	get express() {
		return storage.express || (storage.express=require("./express"));
	},
	/**
	 * @type {module:pig-cmd/file}
	 */
	get file() {
		return storage.file || (storage.file=require("./file"));
	},
	/**
	 * HTTP goodies
	 */
	http: {
		/**
		 * @return {module:pig-cmd/http/request}
		 */
		get request() {
			return storage.http.request || (storage.http.request=require("./http/request"));
		},
		/**
		 * @return {module:pig-cmd/http/response}
		 */
		get response() {
			return storage.http.response || (storage.http.response=require("./http/response"));
		}
	},
	/**
	 * @type {module:pig-cmd/queue}
	 */
	get queue() {
		return storage.queue || (storage.queue=require("./queue"));
	},
	/**
	 * Route goodies.  Why are they included here? Because they operate integrally with the command layer.
	 */
	route: {
		/**
		 * @return {module:pig-cmd/route/factory}
		 */
		get factory() {
			return storage.route.factory || (storage.route.factory=require("./route/factory"));
		},
		/**
		 * @return {module:pig-cmd/route.Route}
		 */
		get Route() {
			return storage.route.Route || (storage.route.Route=require("./route/route").Route);
		}
	},
	/**
	 * Our support suite
	 */
	support: {
		/**
		 * @return {module:pig-cmd/support/spawn}
		 */
		get spawn() {
			return storage.support.spawn || (storage.support.spawn=require("./support/spawn"));
		}
	}
};
